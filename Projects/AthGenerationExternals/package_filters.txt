# Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthGenerationExternals.
#
+ External/CLHEP
+ External/COOL
+ External/CORAL
+ External/CrestApi
+ External/GPerfTools
+ External/Gaudi
+ External/Gdb
+ External/GeoModel
+ External/GoogleTest
+ External/MKL
+ External/onnxruntime
+ External/PyModules
+ External/dSFMT
+ External/flake8_atlas
+ External/prmon
+ External/yampl
- .*
