# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# GitLab-CI configuration for the repository.
#

# Stages for the CI build.
stages:
  - build

# Job-wide variables.
variables:
  EL_PACKAGES: make which wget tar atlas-devel curl-devel libX11-devel libXpm-devel libXft-devel libXext-devel libXi-devel libzstd-devel openssl-devel glib2-devel glibc-devel gmp-devel rpm-build mesa-libGL-devel mesa-libGLU-devel mesa-libEGL-devel libuuid-devel man-db libxkbcommon-devel xz-devel
  ALMA9_PACKAGES: ${EL_PACKAGES} libaio-devel expat-devel libtirpc-devel
  UBUNTU_PACKAGES: git wget lsb-core libx11-dev libxpm-dev libxft-dev libxext-dev libxi-dev libssl-dev libgmp-dev libgl1-mesa-dev libglu1-mesa-dev libegl1-mesa-dev
  CMAKE_VERSION: 3.30.5
  GIT_VERSION: default

# Template for all build jobs.
.build_template: &build_job
  stage: build
  script:
    - echo "Original MAKEFLAGS = $MAKEFLAGS"
    - export MAKEFLAGS="-j8 -l8"
    - echo "Free memory:"
    - free
    - cmake -DCTEST_USE_LAUNCHERS=TRUE ${CMAKE_ARGUMENTS} -S Projects/${PROJECT_NAME} -B ci_build 2>&1 | tee cmake_config.log
    - cmake --build ci_build 2>&1 | tee cmake_build.log
    - ctest --test-dir ci_build -j `nproc` --output-on-failure --output-junit testresults.xml 2>&1 | tee cmake_test.log
  only:
    - main
    - merge_requests
  artifacts:
    when: always
    reports:
      junit: ci_build/testresults.xml
    name: atlasexternals-${PROJECT_NAME}-job-logs-${CI_JOB_ID}
    expose_as: 'Job Logs'
    paths:
      - cmake_config.log
      - cmake_build.log
      - cmake_test.log
      - ci_build/BuildLogs/
    expire_in: 1 week

# Template for all x86_64-el9-gcc13 jobs.
.x86_64_el9_gcc13_template: &x86_64_el9_gcc13_job
  tags:
    - cvmfs
  image: gitlab-registry.cern.ch/linuxsupport/alma9-base:latest
  before_script:
    - yum install -y ${ALMA9_PACKAGES}
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - set +e
    - source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    - asetup none,gcc13
    - lsetup "cmake ${CMAKE_VERSION}"
    - lsetup "git ${GIT_VERSION}"
    - set -e

# Template for all x86_64-ubuntu2004-gcc9 jobs.
.x86_64_ubuntu2004_gcc9_template: &x86_64_ubuntu2004_gcc9_job
  tags:
    - cvmfs
  image: registry.cern.ch/docker.io/library/ubuntu:20.04
  before_script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt-get update
    - apt-get install -y ${UBUNTU_PACKAGES}
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - set +e
    - source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    - lsetup "cmake 3.27.5"
    - set -e
    - export MAKEFLAGS="-j`nproc` -l`nproc`"
    - export LANG=C.UTF-8
    - export LC_ALL=C.UTF-8

#
# x86_64-el9-gcc13 build(s).
#
build:x86_64-el9-gcc13:
  <<: *build_job
  <<: *x86_64_el9_gcc13_job
  parallel:
    matrix:
      - PROJECT_NAME: ['AnalysisBaseExternals', 'AthAnalysisExternals',
                       'AthenaExternals',
                       'AthGenerationExternals', 'AthSimulationExternals',
                       'TestExternals', 'VP1LightExternals']
        CMAKE_ARGUMENTS: ['-DCMAKE_BUILD_TYPE=RelWithDebInfo']

#
# x86_64-ubuntu2004-gcc9 build(s).
#
build:x86_64-ubuntu2004-gcc9:
  <<: *build_job
  <<: *x86_64_ubuntu2004_gcc9_job
  parallel:
    matrix:
      - PROJECT_NAME: ['AnalysisBaseExternals']
        CMAKE_ARGUMENTS: ['-DCMAKE_BUILD_TYPE=Release']
