# Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  SYMPY_PYTHON_PATH
#
# Can be steered by SYMPY_LCGROOT

# The LCG include(s).
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME sympy
   BINARY_NAMES isympy
   BINARY_SUFFIXES bin
   PYTHON_NAMES isympy.py sympy/__init__.py )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( sympy DEFAULT_MSG
   _SYMPY_PYTHON_PATH _SYMPY_BINARY_PATH )

# Set up the RPM dependency.
lcg_need_rpm( sympy )
