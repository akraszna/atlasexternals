# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Module finding SFGen in the LCG release. Defines:
#  - SFGEN_FOUND
#  - SFGEN_INCLUDE_DIR
#  - SFGEN_INCLUDE_DIRS
#  - SFGEN_LIBRARIES
#  - SFGEN_LIBRARY_DIRS
#
# Can be steered by SFGEN_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME SFGen
   BINARY_NAMES SFGen
   BINARY_SUFFIXES bin bin32 bin64
   INCLUDE_SUFFIXES include src/inc
   INCLUDE_NAMES decay.f norm.f
   LIBRARY_SUFFIXES lib lib64
   COMPULSORY_COMPONENTS SFGen )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( SFGen DEFAULT_MSG SFGEN_INCLUDE_DIR
   SFGEN_LIBRARIES SFGEN_LIBRARY_DIRS )
mark_as_advanced( SFGEN_FOUND SFGEN_INCLUDE_DIR SFGEN_INCLUDE_DIRS
   SFGEN_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( SFGen )

# Set the SFGEN environment variable:
if( SFGEN_FOUND )
   set( SFGEN_ENVIRONMENT
      FORCESET SFGENPATH ${SFGEN_LCGROOT} )
endif()
