# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Try to find mpfr.
#
# Defines:
#  - MPFR_FOUND
#  - MPFR_INCLUDE_DIR
#  - MPFR_LIBRARIES
#  - MPFR_LIBRARY_DIRS
#
# Can be steered by MPFR_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME mpfr
   INCLUDE_SUFFIXES include INCLUDE_NAMES mpfr.h
   LIBRARY_SUFFIXES lib
   DEFAULT_COMPONENTS mpfr )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( mpfr DEFAULT_MSG MPFR_INCLUDE_DIR
   MPFR_LIBRARIES MPFR_LIBRARY_DIRS )
mark_as_advanced( MPFR_FOUND MPFR_INCLUDE_DIR
   MPFR_LIBRARIES MPFR_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( mpfr )
