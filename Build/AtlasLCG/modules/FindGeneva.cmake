# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Module finding Geneva in the LCG release. Defines:
#  - Geneva_FOUND
#  - Geneva_INCLUDE_DIR
#  - Geneva_INCLUDE_DIRS
#  - Geneva_LIBRARIES
#  - Geneva_LIBRARY_DIRS
#
# Can be steered by Geneva_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME Geneva
   INCLUDE_SUFFIXES include INCLUDE_NAMES Geneva/version.hpp
   LIBRARY_SUFFIXES lib
   COMPULSORY_COMPONENTS GenevaCore GenevaDriver)

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( Geneva DEFAULT_MSG GENEVA_INCLUDE_DIR
   GENEVA_LIBRARIES )
mark_as_advanced( Geneva_FOUND GENEVA_INCLUDE_DIR GENEVA_INCLUDE_DIRS
   GENEVA_LIBRARIES GENEVA_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( geneva )
