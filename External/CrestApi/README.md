The CrestApi is the CREST Client C++ Library
================

This package builds the CrestApi libraries for ATLAS offline software projects that do not depend
on tdaq-common, but do depend on the CrestApi.

The library's sources are taken from:
https://gitlab.cern.ch/crest-db/CrestApi
