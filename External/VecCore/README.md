VecCore
=======

This package builds VecCore for use by ATLAS's VecGeom build.

The sources are picked up from VecCore's GitHub development site, and
are currently built with default flags. 

