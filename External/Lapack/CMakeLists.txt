# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration file building the LAPACK library.
#

# Declare the name of the package:
atlas_subdir( Lapack )

# Declare where to get LAPACK from.
set( ATLAS_LAPACK_SOURCE
   "URL;http://cern.ch/atlas-software-dist-eos/externals/Lapack/lapack-lite-3.1.1.tgz;URL_MD5;5feace3f4507a92ef822b2e0b50151d7"
   CACHE STRING "The source for LAPACK" )
mark_as_advanced( ATLAS_LAPACK_SOURCE )

# Decide whether / how to patch the LAPACK sources.
set( ATLAS_LAPACK_PATCH "" CACHE STRING "Patch command for LAPACK" )
set( ATLAS_LAPACK_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of LAPACK (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_LAPACK_PATCH ATLAS_LAPACK_FORCEDOWNLOAD_MESSAGE )

# Extend the CMAKE_Fortran_FLAGS variable with the build type specific
# flags, as we only use that variable in setting up the make.inc file.
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   string( TOUPPER ${CMAKE_BUILD_TYPE} _type )
   set( CMAKE_Fortran_FLAGS
      "${CMAKE_Fortran_FLAGS} ${CMAKE_Fortran_FLAGS_${_type}}" )
   unset( _type )
endif()

# Configure LAPACK's makefile:
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/src/cmake-make.inc.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/make.inc" @ONLY )

# Find an appropriate "make" command.
find_program( ATLAS_MAKE_EXE
   NAMES make gmake nmake
   DOC "(GNU) make command to use" )
mark_as_advanced( ATLAS_MAKE_EXE )

# Build LAPACK for the build area:
ExternalProject_Add( Lapack
   PREFIX "${CMAKE_BINARY_DIR}"
   ${ATLAS_LAPACK_SOURCE}
   ${ATLAS_LAPACK_PATCH}
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E copy
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/make.inc"
   "<SOURCE_DIR>/"
   BUILD_COMMAND "${ATLAS_MAKE_EXE}" lapack_install lib
   INSTALL_COMMAND ${CMAKE_COMMAND} -E make_directory
   "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}"
   COMMAND ${CMAKE_COMMAND} -E copy liblapack.a
   "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/"
   COMMAND ${CMAKE_COMMAND} -E copy libtmglib.a
   "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/"
   BUILD_IN_SOURCE 1 )
ExternalProject_Add_Step( Lapack forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_LAPACK_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
add_dependencies( Lapack Blas )
add_dependencies( Package_Lapack Lapack )

# Install LAPACK:
install( FILES "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/liblapack.a"
   "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/libtmglib.a"
   DESTINATION "${CMAKE_INSTALL_LIBDIR}" OPTIONAL )
